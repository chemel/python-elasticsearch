import requests

class ElasticWrapper:
    elasticUrl = None

    def __init__(self, url):
        self.elasticUrl = url

    # Création d'un index
    def createIndex(self, name):

        d = {
            "settings" : {
                "number_of_shards" : 3,
                "number_of_replicas" : 2
            }
        }

        r = requests.put(self.elasticUrl + '/' + name, json=d)
        j = r.json()
        return j        

    # Inserer un document
    def insertDoc(self, index, id, data):
        r = requests.put(self.elasticUrl + '/' + index + '/_doc/' + str(id), json=data)
        j = r.json()
        return j

    # Obtenir un document
    def getDocument(self, index, id):
        r = requests.get(self.elasticUrl + '/' + index + '/_doc/' + str(id))
        j = r.json()
        return j

    # Rechercher un document
    def search(self, index):
        r = requests.get(self.elasticUrl + '/' + index + '/_search')
        j = r.json()
        return j
