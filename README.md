# Python Elasticsearch

Playing with Python and Elasticsearch

## Installation

```
# Download elasticsearch
wget 'https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-amd64.deb'

# Install
sudo dpkg -i elasticsearch-7.6.2-amd64.deb

# Start service (wait 5 sec)
systemctl start elasticsearch

# Testing
curl -X POST http://localhost:9200/
```

## Start playing with curl

```
curl -X PUT 'localhost:9200/entry?pretty' -H 'Content-Type: application/json' -d '
{
    "settings" : {
        "number_of_shards" : 3,
        "number_of_replicas" : 2
    }
}
'
```

Documentation : https://www.elastic.co/guide/en/elasticsearch/reference/7.6/indices-create-index.html

```
curl -X PUT 'localhost:9200/entry/_doc/1?pretty' -H 'Content-Type: application/json' -d '
{
  "name": "John Doe"
}
'
```

Documentation : https://www.elastic.co/guide/en/elasticsearch/reference/7.6/getting-started-index.html

```
curl -X GET 'localhost:9200/entry/_doc/1?pretty'
```

Documentation : https://www.elastic.co/guide/en/elasticsearch/reference/7.6/docs-get.html

```
curl -X GET 'localhost:9200/entry/_search?pretty'
```

Documentation : https://www.elastic.co/guide/en/elasticsearch/reference/7.6/modules-scripting-fields.html

## Writing step by step python scripts

Show `./stepByStep/` folder

## Writing a wrapper for elasticsearch

Show `./ElasticWrapper.py`

To run enter : `python3 run.py`
