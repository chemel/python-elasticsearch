from ElasticWrapper import ElasticWrapper
import json

def printJson(j):
    print(json.dumps(j, indent=4))

w = ElasticWrapper('http://localhost:9200')

index = 'feed'

# Création de l'index
j = w.createIndex(index)
printJson(j)

# Création d'un nouveau document
d = {
    "name": "Alexis CHEMEL"
}
j = w.insertDoc(index, 1, d)
printJson(j)

# Obtenir le document créé
j = w.getDocument(index, 1)
printJson(j)

# Rechercher tout les documents
j = w.search(index)
printJson(j)