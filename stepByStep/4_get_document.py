import config
import requests
import json

id = 1

r = requests.get(config.ENTRY_INDEX_URL + '/_doc/' + str(id))
j = r.json()

print(r.status_code)
print(json.dumps(j, indent=4))
