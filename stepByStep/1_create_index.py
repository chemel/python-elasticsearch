import config
import requests
import json

d = {
    "settings" : {
        "number_of_shards" : 3,
        "number_of_replicas" : 2
    }
}


r = requests.put(config.ENTRY_INDEX_URL, json=d)
j = r.json()

print(r.status_code)
print(json.dumps(j, indent=4))

