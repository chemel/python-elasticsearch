import config
import requests
import json

r = requests.get(config.ENTRY_INDEX_URL + '/_search')
j = r.json()

print(r.status_code)
print(json.dumps(j, indent=4))
