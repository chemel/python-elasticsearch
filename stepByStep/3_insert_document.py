import config
import requests
import json

id = 1

d = {
    "name": "Alexis CHEMEL"
}

r = requests.put(config.ENTRY_INDEX_URL + '/_doc/' + str(id), json=d)
j = r.json()

print(r.status_code)
print(json.dumps(j, indent=4))
